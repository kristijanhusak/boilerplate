<?php
include 'functions.php';

$pages = include "pages_array.php";
$page = '';

if (in_array($_GET['page'], array_keys($pages)))
{
    $page = $_GET['page'].'.php';
}
else
{
    setFlashMessage('No page with that name!');
    header('Location: index.php');
}

include_once "header.php";
include $page;
include_once "footer.php";

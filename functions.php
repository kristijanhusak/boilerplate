<?php
session_start();

/**
 * Print the link for page
 *
 * @param string $page Page file name
 * @param string $pageName Page description
 *
 * @return string
 */
function printPageLink($page, $pageName)
{
    echo '<h2><a href="pages.php?page='.$page.'">'.$pageName.'</a></h2>';
}

/**
 * set the flash message
 *
 * @param string $msg Message text
 * @return void
 */
function setFlashMessage($msg, $type = 'e')
{
    $_SESSION['pa_error']['msg'] = $msg;
    $_SESSION['pa_error']['type'] = $type;
}

/**
 * Print the flash message from session
 *
 * @return string|void
 */
function printFlashMessages()
{
    if (isset($_SESSION['pa_error']) && !empty($_SESSION['pa_error']))
    {
        $color = ( $_SESSION['pa_error']['type'] == 'e' ) ? 'red' : 'green';
        echo '<h3 style="color: '.$color.';">'.$_SESSION['pa_error']['msg'].'</h3>';
        unset($_SESSION['pa_error']);
    }
}

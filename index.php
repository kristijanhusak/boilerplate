<?php
include 'functions.php';

$pages = include 'pages_array.php';

printFlashMessages();

if (count($pages) > 0)
{
    foreach ($pages as $page => $pageName)
    {
        printPageLink($page, $pageName);
    }
}
else
{
    echo 'No pages.';
}
include_once 'footer.php';



define(['views/global'], function(Global) {
  describe("Global setup", function() {

    // jasmine.getFixtures().fixturesPath = '/base/templates'
    // loadFixtures('test.html')

    var global;

    // Instantiate on load
    beforeEach(function () {
        global = new Global({});
    });


    it("Should have hide address bar function defined", function() {
        expect(global.hideAddressBar).toBeDefined();
    });

    it("Should have default data as empty object", function() {
        expect(global.data).toEqual({});
    });

    it("Should set proper data", function() {
        var glob = new Global({ test: 'tested' });
        expect(glob.data).toEqual({ test: 'tested' });
        expect(glob.data.test).toEqual('tested');
    });
  });
});

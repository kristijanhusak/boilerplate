define(['backbone'], function (Backbone) {
    var Global = Backbone.View.extend({

        initialize: function (data) {
            this.data = data;
            this.hideAddressBar();
        },

        hideAddressBar: function () {
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        }
    });

    return Global;
});

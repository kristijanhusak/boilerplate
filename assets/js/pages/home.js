/*
|------------------------------------------------------------
| This is the main file for home page
|------------------------------------------------------------
*/

define(['backbone'], function (Backbone) {

    var Home = Backbone.View.extend({

        initialize: function (global) {
            this.global = global;
        }
    });

	return Home;
});

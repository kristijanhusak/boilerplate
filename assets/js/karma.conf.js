// Karma configuration
// Generated on Fri Feb 28 2014 10:33:24 GMT+0100 (CET)

module.exports = function(config) {
  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '',


    // frameworks to use
    frameworks: ['jasmine', 'requirejs', 'sinon'],


    // list of files / patterns to load in the browser
    files: [
      '../css/style.css',
      {pattern: 'vendor/**/*.js', included: false},
      {pattern: 'views/**/*.js', included: false},
      {pattern: 'pages/**/*.js', included: false},
      {pattern: 'models/**/*.js', included: false},
      {pattern: 'collections/**/*.js', included: false},
      {pattern: 'templates/*', watched: true, served: true, included: false},
      {pattern: 'tests/libs/**/*.js', included: false},
      {pattern: 'tests/**/*Spec.js', included: false},
      'tests/test-main.js',
    ],


    // list of files to exclude
    exclude: [

    ],

    preprocessors: {
      '**/*.html': []
    },


    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress', 'growl-notifications'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: ['Chrome'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};

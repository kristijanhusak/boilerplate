({
	appDir: '../../',
	baseUrl: 'assets/js/',
	mainConfigFile: 'common.js',
	dir: '../../www-release',
	modules: [
		{
			name: 'common',
			include: ['backbone', 'views/global']
		},

		{
			name: 'pages/home',
			exclude: ['backbone']
		}
	]
})

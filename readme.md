# Boilerplate

This is boilerplate for starting new front end projects

### Javascript libraries
1. jQuery
2. RequireJS
3. Backbone
4. Underscore

#Installation

### Javascript

For the main Javascript dependencies installation the package `Volo` is needed.
It can be installed via NPM (nodejs) like this:

`npm install -g volo`

after the installation just do

`volo add`

in your project folder


## Folders

### assets
Folder for holding all things related to web page (Images, Fonts, JS files , Css)

### Sass
Folder that holds all Compass(Sass) Partials that are compiled to main css file in `assets/css/style.css`


## Files

### index.php

Page for listing all available pages


### pages.php

Page that handles including partials and request page from index.php


### pages_array.php

This page stores an php array of all available pages that will be listen on index.php.
Index in array is page file name, and value is description that will be shown on index.php

### functions.php

File for holding all helper functions for php


## Tests

In case you want to test your Javascript, here's the installation guide:

in the folder `assets/js/tests` install all NPM dependencies with:

`npm install`

and after that we need to install one more dependency with Volo:

`volo add`

To run your tests just run the `test.sh` script in the `assets/js/tests` folder like this:

`./test.sh`

In case you can't run this change the permission of this script to allow executing it. You can do that with this command:

`sudo chmod a+x test.sh`
